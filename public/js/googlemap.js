
var url = window.location.href;
var new_url=url.split('/'); // make it an array
new_url.pop(); // remove the last part of the url
url=new_url.join('/'); // join them together again like the normal url




if(  url == "http://localhost:8000/missings/editMissing/" || url == "http://127.0.0.1:8000/missings/editMissing/" || url == "http://35.197.151.177/missings/editMissing"){
  var temp_lat = parseFloat($("#lat").val());
  var temp_lng = parseFloat($("#lng").val());
  var Missing = {lat: temp_lat  , lng: temp_lng}; //used for editing missing person
  function  initAutocomplete(){// map for editing of missing person record

    var map = new google.maps.Map(document.getElementById('mapEditMissing'),{
      center: Missing,
      zoom: 15,
      styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ],
    });

      var marker = new google.maps.Marker(
          {
              position: map.center,
              map: map,
              draggable: true,
          }
      );


        var input = document.getElementById('searchbox'); //1
        var autocomplete = new google.maps.places.Autocomplete(input); //2

        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace().geometry.location;//3
            //4
            var newmapcenter = {lat:place.lat() ,lng:place.lng() }

            // if i will use map object to set center
            // use map.setCenter
            //
            // if i will use marker object to set center
            // use marker.setPosition

            map.setCenter(newmapcenter);
            marker.setPosition(newmapcenter);
            $('#lat').val(place.lat());
            $('#lng').val(place.lng());
        });

    $("#mapEditMissing").click(function() {
      $("#lat").val(marker.getPosition().lat());
      $("#lng").val(marker.getPosition().lng());
    });


  }// map for editing of missing person record



} //  if





else if(  url == "http://localhost:8000/missings/createMissing/" || url == "http://127.0.0.1:8000/missings/createMissing/" || url == "http://35.197.151.177/missings/createMissing" ) {
    // ------------------------------------------------------------------
    var Cebu = {lat: 10.3157, lng: 123.8854};  //used for creating a new missing person

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapCreateMissing'), {
            center: Cebu,
            zoom: 13,
            mapTypeId: 'roadmap',
            styles: [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#38414e'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#9ca5b3'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#746855'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ],
        });

        var marker = new google.maps.Marker(
            {
                position: map.center,
                map: map,
                draggable: true,
            }
        );

        var input = document.getElementById('searchbox'); //1
        var autocomplete = new google.maps.places.Autocomplete(input); //2

        autocomplete.addListener('place_changed', function() {

            var place = autocomplete.getPlace().geometry.location; // shorten the way to get the lat and lng for the selected place in the searchbox... 3
            // console.log(place);
            // console.log("lat ="+place.lat()+" & lng ="+place.lng());
            //4
            var newmapcenter = {lat:place.lat() ,lng:place.lng() }

            // if i will use map object to set center
            // use map.setCenter
            //
            // if i will use marker object to set center
            // use marker.setPosition
            map.setCenter(newmapcenter);
            marker.setPosition(newmapcenter);
            $('#lat').val(place.lat());
            $('#lng').val(place.lng());
        });

        $(document).ready(function () {
            $("#lat").val(marker.getPosition().lat());
            $("#lng").val(marker.getPosition().lng());

            $("#mapCreateMissing").click(function () {
                $("#lat").val(marker.getPosition().lat());
                $("#lng").val(marker.getPosition().lng());
            });
        });


    }
} //else if

else if(  url == 'http://localhost:8000/sightings/detail/' || url == 'http://127.0.0.1:8000/sightings/detail/' || url == 'http://35.197.151.177/sightings/detail' ) {
  var temp_lat = parseFloat($("#lat").val());
  var temp_lng = parseFloat($("#lng").val());
  var Sighting = {lat: temp_lat  , lng: temp_lng}; //used for viewing the sighting of a missing person
  function initAutocomplete() {
    var map = new google.maps.Map(document.getElementById('mapSightingDetail'),{
      center: Sighting,
      zoom:15,
      styles: [
            {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
            {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
            {
              featureType: 'administrative.locality',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'geometry',
              stylers: [{color: '#263c3f'}]
            },
            {
              featureType: 'poi.park',
              elementType: 'labels.text.fill',
              stylers: [{color: '#6b9a76'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry',
              stylers: [{color: '#38414e'}]
            },
            {
              featureType: 'road',
              elementType: 'geometry.stroke',
              stylers: [{color: '#212a37'}]
            },
            {
              featureType: 'road',
              elementType: 'labels.text.fill',
              stylers: [{color: '#9ca5b3'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry',
              stylers: [{color: '#746855'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'geometry.stroke',
              stylers: [{color: '#1f2835'}]
            },
            {
              featureType: 'road.highway',
              elementType: 'labels.text.fill',
              stylers: [{color: '#f3d19c'}]
            },
            {
              featureType: 'transit',
              elementType: 'geometry',
              stylers: [{color: '#2f3948'}]
            },
            {
              featureType: 'transit.station',
              elementType: 'labels.text.fill',
              stylers: [{color: '#d59563'}]
            },
            {
              featureType: 'water',
              elementType: 'geometry',
              stylers: [{color: '#17263c'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.fill',
              stylers: [{color: '#515c6d'}]
            },
            {
              featureType: 'water',
              elementType: 'labels.text.stroke',
              stylers: [{color: '#17263c'}]
            }
          ],
    });

    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    geocodeLatLng(geocoder, map, infowindow);

  }

    function geocodeLatLng(geocoder, map, infowindow) {

                            // search for a possible result with a given lat and lng and its function expects to return a result and a status
       geocoder.geocode({'location': Sighting}, function(results, status) {

            if (status === 'OK') {
                if (results[0]) {
                    map.setZoom(17);
                    var marker = new google.maps.Marker({
                        position: Sighting,
                        map: map
                    });
                    infowindow.setContent("Sighted in: "+results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });

    }

}else if(url == "http://35.197.151.177/missings/missingPersonDetail" ) {

    var temp_lat = parseFloat($("#lat").val());
    var temp_lng = parseFloat($("#lng").val());
    var Missing = {lat: temp_lat  , lng: temp_lng}; //used for viewing the sighting of a missing person

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('mapMissingDetail'),{
            center: Missing,
            zoom:15,
            styles: [
                {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
                {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
                {
                    featureType: 'administrative.locality',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'geometry',
                    stylers: [{color: '#263c3f'}]
                },
                {
                    featureType: 'poi.park',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#6b9a76'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry',
                    stylers: [{color: '#38414e'}]
                },
                {
                    featureType: 'road',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#212a37'}]
                },
                {
                    featureType: 'road',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#9ca5b3'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry',
                    stylers: [{color: '#746855'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'geometry.stroke',
                    stylers: [{color: '#1f2835'}]
                },
                {
                    featureType: 'road.highway',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#f3d19c'}]
                },
                {
                    featureType: 'transit',
                    elementType: 'geometry',
                    stylers: [{color: '#2f3948'}]
                },
                {
                    featureType: 'transit.station',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#d59563'}]
                },
                {
                    featureType: 'water',
                    elementType: 'geometry',
                    stylers: [{color: '#17263c'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.fill',
                    stylers: [{color: '#515c6d'}]
                },
                {
                    featureType: 'water',
                    elementType: 'labels.text.stroke',
                    stylers: [{color: '#17263c'}]
                }
            ],
        });
        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;
        geocodeLatLng(geocoder, map, infowindow);
    }

    function geocodeLatLng(geocoder, map, infowindow) {
        geocoder.geocode({'location': Missing}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    map.setZoom(15);
                    var marker = new google.maps.Marker({
                        position: Missing,
                        map: map,
                        draggable:false,
                    });
                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(map, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

}


else if(url == "http://35.197.151.177/missings/print_details" ) {

    var temp_lat = parseFloat($("#lat").val());
    var temp_lng = parseFloat($("#lng").val());
    var Missing = {lat: temp_lat  , lng: temp_lng}; //used for viewing the sighting of a missing person

    function initAutocomplete() {
        var geocoder = new google.maps.Geocoder;

        geocodeLatLng(geocoder);
    }

    function geocodeLatLng(geocoder, map, infowindow) {
        geocoder.geocode({'location': Missing}, function(results, status) {
            if (status === 'OK') {
                $("#lkl").html(results[0].formatted_address);
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }
}


else {
  console.log("Nothing to load here. ");
}













































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































//
//
//
// var temp_lat = parseFloat($("#lat").val());
// var temp_lng = parseFloat($("#lng").val());
//
//
// var Cebu = {lat : 10.3157 , lng : 123.8854};  //used for creating a new missing person
// var Missing = {lat: temp_lat  , lng: temp_lng}; //used for editing missing person
//
// var url = window.location.href;
// // remove the last character
// url = url.slice(0, -1)
//
//
//
// if( url == "http://localhost:8000/missings/editMissing/"){
//
//   function  initMap(){// map for editing of missing person record
//
//     var map = new google.maps.Map(document.getElementById('mapEditMissing'),{
//       center: Missing,
//       zoom: 10,
//       styles: [
//             {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
//             {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
//             {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
//             {
//               featureType: 'administrative.locality',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'poi',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'poi.park',
//               elementType: 'geometry',
//               stylers: [{color: '#263c3f'}]
//             },
//             {
//               featureType: 'poi.park',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#6b9a76'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'geometry',
//               stylers: [{color: '#38414e'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'geometry.stroke',
//               stylers: [{color: '#212a37'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#9ca5b3'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'geometry',
//               stylers: [{color: '#746855'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'geometry.stroke',
//               stylers: [{color: '#1f2835'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#f3d19c'}]
//             },
//             {
//               featureType: 'transit',
//               elementType: 'geometry',
//               stylers: [{color: '#2f3948'}]
//             },
//             {
//               featureType: 'transit.station',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'geometry',
//               stylers: [{color: '#17263c'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#515c6d'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'labels.text.stroke',
//               stylers: [{color: '#17263c'}]
//             }
//           ]
//     });
//
//     var marker = new google.maps.Marker({
//       position : map.center,
//       map : map,
//       draggable : true,
//     });
//
//         var input = document.getElementById('searchbox'); //1
//         var autocomplete = new google.maps.places.Autocomplete(input); //2
//
//         autocomplete.addListener('place_changed', function() {
//             var place = autocomplete.getPlace();//3
//             //4
//             var newmapcenter = {lat:place.geometry.location.lat() ,lng:place.geometry.location.lng() }
//
//             // if i will use map object to set center
//             // use map.setCenter
//             //
//             // if i will use marker object to set center
//             // use marker.setPosition
//
//             map.setCenter(newmapcenter);
//             marker.setPosition(newmapcenter);
//             $('#lat').val(marker.getPosition().lat());
//             $('#lng').val(marker.getPosition().lng());
//         });
//
//     $("#mapEditMissing").click(function(event) {
//       $("#lat").val(marker.getPosition().lat());
//       $("#lng").val(marker.getPosition().lng());
//     });
//
//
//   }// map for editing of missing person record
//
//
//
// } //  if
//
//
//
//
//
// else if(url == 'http://localhost:8000/missings/createMissing/') {
//   // ------------------------------------------------------------------
//   function initMap() {// map for creation of a missing person record
//     var map = new google.maps.Map(document.getElementById('mapCreateMissing'), {
//       center: Cebu,
//       zoom: 10,
//       styles: [
//             {elementType: 'geometry', stylers: [{color: '#242f3e'}]},
//             {elementType: 'labels.text.stroke', stylers: [{color: '#242f3e'}]},
//             {elementType: 'labels.text.fill', stylers: [{color: '#746855'}]},
//             {
//               featureType: 'administrative.locality',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'poi',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'poi.park',
//               elementType: 'geometry',
//               stylers: [{color: '#263c3f'}]
//             },
//             {
//               featureType: 'poi.park',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#6b9a76'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'geometry',
//               stylers: [{color: '#38414e'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'geometry.stroke',
//               stylers: [{color: '#212a37'}]
//             },
//             {
//               featureType: 'road',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#9ca5b3'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'geometry',
//               stylers: [{color: '#746855'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'geometry.stroke',
//               stylers: [{color: '#1f2835'}]
//             },
//             {
//               featureType: 'road.highway',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#f3d19c'}]
//             },
//             {
//               featureType: 'transit',
//               elementType: 'geometry',
//               stylers: [{color: '#2f3948'}]
//             },
//             {
//               featureType: 'transit.station',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#d59563'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'geometry',
//               stylers: [{color: '#17263c'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'labels.text.fill',
//               stylers: [{color: '#515c6d'}]
//             },
//             {
//               featureType: 'water',
//               elementType: 'labels.text.stroke',
//               stylers: [{color: '#17263c'}]
//             }
//           ]
//     });
//
//     var marker = new google.maps.Marker({
//       position : map.center,
//       map : map,
//       draggable : true,
//
//     });
//     var input = document.getElementById('searchbox');
//     var autocomplete = new google.maps.places.Autocomplete(input);
//                   // add an event listen using the google addListener
//                   autocomplete.addListener('place_changed', function() {
//                       var place = autocomplete.getPlace();
//                       var newmapcenter = {lat:place.geometry.location.lat() ,lng:place.geometry.location.lng() }
//                       // set the map center
//                       map.setCenter(newmapcenter);
//                       // set the position of the marker according to the new center basing on the place that is being searched
//                       marker.setPosition(newmapcenter);
//                       $('#lat').val(marker.getPosition().lat());
//                       $('#lng').val(marker.getPosition().lng());
//                   });
//
//     $(document).ready(function() {
//       $('#lat').val(map.center.lat());
//       $('#lng').val(map.center.lng());
//
//               $("#mapCreateMissing").click(function(event) {
//                 $('#lat').val(marker.getPosition().lat());
//                 $('#lng').val(marker.getPosition().lng());
//               });
//     });
//
//
//   } // map for creation of a missing person record
//
//
//
// } //else
