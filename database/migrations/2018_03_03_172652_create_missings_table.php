<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missings', function (Blueprint $table) {
            $table->increments('id');
//            file name of the image, this is used for the web application
            $table->string('avatar')->default('default.png');
//            used to access an image for the android application
            $table->string('imgurl')->default('http://35.197.151.177/findme/uploads/missings/default.png	');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('birthdate');
            $table->string('gender');
            $table->string('nationality');
            $table->string('lat');
            $table->string('lng');
            $table->string('date_last_seen');
            $table->string('skin_type');
            $table->string('height');
            $table->string('weight');
            $table->string('medical_history');
            $table->string('body_marks');
            $table->string('top');
            $table->string('bottom');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missings');
    }
}
