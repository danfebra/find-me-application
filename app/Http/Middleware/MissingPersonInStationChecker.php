<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Incident;

class MissingPersonInStationChecker
{
    /**
     * Checks if the missing person is in the same station with current authenticated user.
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //    gets the last character of the URL which is the missing ID
        $url = $request->url();
        if(preg_match("/\/(\d+)$/",$url,$matches))
        {
            $missing_id=$matches[1];
        }

        // we used incident model because the incidents table holds the station of the missing person
        $incident = Incident::where('missing_id',$missing_id)->first();

        if($incident == null || $incident->station_id != Auth::guard('officer')->user()->station_id){
          return redirect()->back();
        }
        else {
          return $next($request);
        }
    }
}
