<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Officer;

class OfficerInStationChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        this will check if the officer that is being edited by the head officer is in the same station
        $url = $request->url();
        if(preg_match("/\/(\d+)$/",$url,$matches))
        {
            $officer_id=$matches[1];
        }

        $officer = Officer::find($officer_id);

        if($officer == null || $officer->station_id != Auth::guard('officer')->user()->station_id){
          return redirect()->back();

        }
        else {
          return $next($request);
        }
    }
}
