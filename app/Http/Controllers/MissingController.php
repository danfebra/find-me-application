<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Hash;
use Auth;
use Session;
use Redirect;
use Image;
use Storage;
use PDF;
use Mail;
use App\Incident;
use App\Petitioner;
use App\Missing;
class MissingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $petitioner = Petitioner::find($id);
        return view('missing.missing_create',['petitioner' => $petitioner]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
      $missing = new Missing();

      $incident = new Incident();

      if($request->hasFile('avatar')){
        $this->validate($request,['avatar'=>'image|required']);

        // get image
        $avatar = $request->file('avatar');
        // change file name
        $filename = time().'.'.$avatar->getClientOriginalExtension();
        // make the image for web
        Image::make($avatar)->resize(300,300)->save(public_path('uploads/missings/'.$filename));
          // make the image for mobile
        Image::make($avatar)->resize(300,300)->save(public_path('findme/uploads/missings/'.$filename));

        //used for db only not the actual saving of image for android
          $missing->imgurl = 'http://35.197.151.177/findme/uploads/missings/'.$filename;
        // for web
          $missing->avatar = $filename;

      }


      // first validate all fields
      $this->validate($request,[
        'first_name'=>'required',
        'middle_name'=>'nullable',
        'last_name'=>'required',
        'birthdate'=>'required',
        'gender'=>'required',
        'nationality'=>'required',
        'lat'=>'required',
        'lng'=>'required',
        'date_last_seen'=>'required',
        'skin_type'=>'required',
        'height'=>'required',
        'weight'=>'required',
        'body_marks'=>'nullable',
        'medical_history'=>'required',
        'top'=>'required',
        'bottom'=>'required',
      ]);
        if($request->body_marks == "None"){
            $missing->body_marks = $request->body_marks;
            $missing->description = $request->description."\nPerson has no tattoo.";
        }
        else{
            $missing->body_marks = $request->body_marks;
            $missing->description = $request->description;
        }
      $missing->first_name = $request->first_name;
      ($request->middle_name == null || $request->middle_name == "")? $missing->middle_name ="" :$missing->middle_name = $request->middle_name;
      $missing->last_name = $request->last_name;
      $missing->birthdate = $request->birthdate;
      $missing->gender = $request->gender;
      $missing->nationality = $request->nationality;
      $missing->lat= $request->lat;
      $missing->lng= $request->lng;
      $missing->date_last_seen = $request->date_last_seen;
      $missing->skin_type = $request->skin_type;
      $missing->height = $request->height;
      $missing->weight = $request->weight;
      $missing->medical_history = $request->medical_history;
      $missing->top = $request->top;
      $missing->bottom = $request->bottom;
      $missing->save();

      $incident->petitioner_id = $id;
      $incident->missing_id=$missing->id;
      $incident->station_id = Auth::guard('officer')->user()->station_id;
      $incident->officer_id = Auth::guard('officer')->user()->id;
      $incident->status = 1;
      $incident->save();


        Session::flash('msg','A new incident has been added!');


      //alert the android app user if a new incident has been created
        function send_notification ($tokens, $message)
        {

            $url = 'https://fcm.googleapis.com/fcm/send';
            $fields = array(
                'registration_ids' => $tokens,
                'data' => $message
            );
            $headers = array(
                'Authorization:key = AAAA-UUc1uk:APA91bEw-ajWFV9-8zClTXq8Rhh9WJwwtC3dohNaaRW9miryPY4Ur5ezyU2y1yAmcD6hsZ1f_mTYxQ9YpqMlXbunztbbWqZMeOAlkSaya7h6dG0TuGvalKdklj5xNYmGhfbQunOf4Uk1 ',
                'Content-Type: application/json'
            );
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            if ($result === FALSE) {
                die('Curl failed: ' . curl_error($ch));
            }
            curl_close($ch);
            return $result;
        }

        $conn = mysqli_connect("localhost","root","findmeapp","findmeapp");
        $conn->set_charset("utf8");
        $sql = " Select token FROM devices ";
        $result = mysqli_query($conn,$sql);
        $tokens = array();

        if(mysqli_num_rows($result) > 0 ){
            while ($row = mysqli_fetch_assoc($result)) {
                $tokens[] = $row["token"];
            }
        }

        mysqli_close($conn);
        $message = array("message" => $missing->first_name." ".$missing->last_name." has been added. Please check your devices.");
        $message_status = send_notification($tokens, $message);


        return redirect('/officers');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function missingPersonDetails($id)
    {
//      used first() because i want to get the first result of the query, i can't use find because i did not pass the incident id.
//      the id that was passed was the missing person's id

        $missing = Incident::with(['petitioner','station','missing'])->where('missing_id',$id)->first();
         if($missing->flag == 0){
             return redirect()->back();
         }
         else{
             return view('missing.missing_details',['missing'=>$missing]);
         }


    }
    public function missingPrintDetails($id)
    {
//      used first() because i want to get the first result of the query, i can't use find because i did not pass the incident id.
//      the id that was passed was the missing person's id

        $missing = Incident::with(['petitioner','station','missing'])->where('missing_id',$id)->first();
        if($missing->flag == 0){
            return redirect()->back();
        }
        else{
            $pdf = PDF::loadview('missing.missing_print_details',['missing'=>$missing]);
            return $pdf->download('missing_'.$missing->missing->first_name.'_'.$missing->missing->last_name.'.pdf');
//            return view('missing.missing_print_details',['missing'=>$missing]);
        }



    }

    public function foundORmissing($id)
    {
        $missing = Incident::where('missing_id',$id)->first();

        if($missing->status == 0 && $missing->flag == 0){
            return redirect()->back();
        }
        else{
            $missing->status = 0;
            $missing->save();
            return redirect('/officers');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function delete(Request $request, $id)
    {
        $missing = Incident::find($id);
        $missing->flag = $request->flag;
        $missing->save();
        Session::flash('msg','An incident has been deleted!');
        return redirect()->back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */

    public function edit($id)
    {
        $missing = Missing::find($id);
        return view('missing.missing_edit',['missing'=>$missing]);
    }


    public function update(Request $request, $id)
    {
        $missing = Missing::find($id);
        // first validate all fields
        $this->validate($request,[
            'first_name'=>'required',
            'middle_name'=>'nullable',
            'last_name'=>'required',
            'birthdate'=>'required',
            'gender'=>'required',
            'nationality'=>'required',
            'lat'=>'required',
            'lng'=>'required',
            'date_last_seen'=>'required',
            'skin_type'=>'required',
            'height'=>'required',
            'weight'=>'required',
            'medical_history'=>'required',
            'body_marks'=>'nullable',
            'top'=>'required',
            'bottom'=>'required',
        ]);

        if($request->body_marks == "None"){
            $missing->body_marks = $request->body_marks;
            $missing->description = $request->description;
        }
        else{
            $missing->body_marks = $request->body_marks;
            $missing->description = $request->description;
        }

        $missing->first_name = $request->first_name;
        ($request->middle_name == null || $request->middle_name == "")? $missing->middle_name="" : $missing->middle_name = $request->middle_name;
        $missing->last_name = $request->last_name;
        $missing->birthdate = $request->birthdate;
        $missing->gender = $request->gender;
        $missing->nationality = $request->nationality;
        $missing->lat= $request->lat;
        $missing->lng= $request->lng;
        $missing->date_last_seen = $request->date_last_seen;
        $missing->skin_type = $request->skin_type;
        $missing->height = $request->height;
        $missing->weight = $request->weight;
        $missing->medical_history = $request->medical_history;
//        $missing->body_marks = $request->body_marks;
        $missing->top = $request->top;
        $missing->bottom = $request->bottom;
//        $missing->description = $request->description;
        $missing->save();
        Session::flash('msg','You have successfully updated the missing person\'s information');
        return redirect()->back();


    }

    public function updateAvatar(Request $request,$id)
    {
        $missing = Missing::find($id);
        if($request->hasFile('avatar')){
            $this->validate($request,['avatar'=>'image|required']);
            $avatar = $request->file('avatar');
            $filename=time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300,300)->save(public_path('uploads/missings/'.$filename));
            Image::make($avatar)->resize(300,300)->save(public_path('findme/uploads/missings/'.$filename));
            $missing->avatar = $filename;
            $missing->imgurl = 'http://35.197.151.177/findme/uploads/missings/'.$filename;
            $missing->save();
            Session::flash('msg','Successfully updated the image.');
            return redirect()->back();
        }
        else{
            Session::flash('msg','There was no selected image, can\'t make the update.');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showAllDeleted()
    {
        $missings = Incident::with(['missing','petitioner'])->where('station_id',Auth::guard('officer')->user()->station_id)->where('flag',0)->get();
        return view('missing.missing_showall_deleted',['missings'=>$missings]);
    }
    public function restoreMissing($id)
    {
        $missing = Incident::where('missing_id',$id)->first();
        $missing->flag = 1;
        $missing->save();
        Session::flash('msg','Missing Person was successfully restored');
        return redirect()->back();
    }
}