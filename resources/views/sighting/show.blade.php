@extends('app.template')
@section('content')
  @if(Session::has('msg'))
    <div class="alert alert-info">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Heads Up!</strong> {{Session::get('msg')}}
    </div>
  @endif

  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Sighting Reports for {{$station_name}} Incidents</h3>
    </div>
    <div class="box-body">

      <table id="Table" class="table table-bordered table-striped table-hover table-responsive">
        <thead>
        <tr>
          <th>Missing Person's Information</th>
          <th>Reported By</th>
          <th>Contact #</th>
          <th>Status</th>
          <th>Date Sighted</th>
          <th>Action</th>

        </tr>
        </thead>
        <tbody>
        @foreach($sightings as $sighting)
          <tr>
            <td><img src="{{asset('/uploads/missings/'.$sighting->incident->missing->avatar)}}" width="80" height="80" alt=""> {{ $sighting->incident->missing->first_name }} &nbsp {{ $sighting->incident->missing->last_name }} </td>
            <td>{{ $sighting->first_name }} &nbsp {{ $sighting->last_name }} </td>
            <td>{{$sighting->contact_no}}</td>
            @if($sighting->status==0)
            <td>Pending</td>
            @elseif($sighting->status==1)
              <td>Accepted</td>
            @else
              <td>Rejected</td>
            @endif
            <td>{{$sighting->created_at}}</td>
            <td>
                  <center>

                    <!-- --------------------------------------------------- -->
                @if($sighting->status==0)
                    <form action="{{url('sightings/accept/'.$sighting->id)}}" method="post">
                      {{csrf_field()}}
                      <button href="" onclick="activation(event)" class="btn btn-md btn-success btn-block" name="button">Accept</button>
                    </form>

                      <br>
                    <!-- --------------------------------------------------- -->

                    <form action="{{url('sightings/decline/'.$sighting->id)}}" method="post">
                      {{csrf_field()}}
                      <button href="" onclick="activation(event)" class="btn btn-md btn-danger btn-block"  name="button">Decline</button>
                    </form>
                  @endif
                    <!-- --------------------------------------------------- -->
                    <br>
                    <a href="{{url('sightings/detail/'.$sighting->id)}}" class="btn btn-md btn-primary btn-block"  name="button">View</a>
                  </center>
            </td>

          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>

@endsection
