@extends('app.template')
@section('content')
<div class="box">
  <div class="box-header">
    <h3 class="box-title">Sighting Details</h3>
  </div>
  <div class="box-body">
    <div class="container-fluid">

      <!-- SIGHTING IMAGE -->
      <div class="row">
        <div class="col-xs-6">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sighting Image</h3>
                <center>
                  <img src="{{asset('findme/uploads/sightings/'.$sighting->imgurl)}}" class="img-circle" alt="User Image" height="400px" width="400px" style="box-shadow: 10px 10px 50px grey;">
                </center>
            </div>
          </div>
        </div>




        <!-- REPORTED BY -->
          <div class="col-xs-6">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">Missing Person</h3>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-4">
                    {{Form::bsText(null,'First Name',$sighting->incident->missing->first_name,['readonly'=>'readonly'])}}
                  </div>
                  <div class="col-xs-4">
                    {{Form::bsText(null,'First Name',$sighting->incident->missing->middle_name,['readonly'=>'readonly'])}}
                  </div>
                  <div class="col-xs-4">
                    {{Form::bsText(null,'First Name',$sighting->incident->missing->last_name,['readonly'=>'readonly'])}}
                  </div>
                </div>
              </div>
            </div>
            <div class="box">
              <div class="box-header">
                 <h3 class="box-title">Sighted By</h3>
              </div>
              <div class="box-body">
                  <div class="row">
                    <div class="col-xs-4">
                      {{ Form::bsText(null,'First Name',$sighting->first_name,['readonly'=>'readonly']) }}
                    </div>
                    <div class="col-xs-4">
                      {{ Form::bsText(null,'Last Name',$sighting->last_name,['readonly'=>'readonly']) }}
                    </div>
                    <div class="col-xs-4">
                      {{ Form::bsText(null,'Contact #',$sighting->contact_no,['readonly'=>'readonly']) }}
                    </div>
                  </div>
                <div class="row">
                  <div class="col-xs-6">
                    <!-- --------------------------------------------------- -->
                    @if($sighting->status==0)
                      <form action="{{url('sightings/accept/'.$sighting->id)}}" method="post">
                        {{csrf_field()}}
                        <button href="" onclick="activation(event)" class="btn btn-md btn-success btn-block" name="button">Accept</button>
                      </form>
                  </div>
                  <div class="col-xs-6">
                    <form action="{{url('sightings/decline/'.$sighting->id)}}" method="post">
                      {{csrf_field()}}
                      <button href="" onclick="activation(event)" class="btn btn-md btn-danger btn-block"  name="button">Decline</button>
                    </form>
                    @endif
                  </div>
                </div>
              </div>
            </div>
          </div> <!-- REPORTED BY DIV -->
        </div><!-- ROW -->
    <!-- map -->
      <div class="row">
        <input hidden type="text" name="" id="lat" value="{{$sighting->lat}}">
        <input hidden type="text" name="" id="lng" value="{{$sighting->lng}}">
        <div id="mapSightingDetail" style="height:50vh;">
        </div>
      </div>

    </div> <!-- CONTAINER -->
    </div>
</div> <!-- BOX -->
@endsection
