@extends('app.template')
@section('content')
    <div class="box">
        <div class="box-header">
            <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <center><img src="/uploads/missings/{{$missing->missing->avatar}}" alt=""></center>
                </div>
                <div class="col-xs-4 col-xs-offset-4">
                    <center ><h1>{{$missing->status? "MISSING":"FOUND"}}</h1></center>
                </div>
            </div>
            <br>
            <br>
            <div class="row">
                <div class="col-xs-4">
                    {{ Form::bsText(null,'First Name',$missing->missing->first_name,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Middle Name',$missing->missing->middle_name,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Last Name',$missing->missing->last_name,['readonly'=>'readonly']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-3">
                    {{ Form::bsDate(null,'Date of Birth',$missing->missing->birthdate,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-3">
                    {{ Form::bsText(null,'Gender',$missing->missing->gender,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-3">
                    {{ Form::bsDate(null,'Date Last Seen',$missing->missing->date_last_seen,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-3">
                    {{ Form::bsText(null,'Nationality',$missing->missing->nationality,['readonly'=>'readonly']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Skin Type',$missing->missing->skin_type,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-2">
                    {{ Form::bsText(null,'Heigth',$missing->missing->height,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-2">
                    {{ Form::bsText(null,'Weight',$missing->missing->weight,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Medical History',$missing->missing->medical_history,['readonly'=>'readonly']) }}
                </div>
            </div>

            <div class="row">
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Body Marks',$missing->missing->body_marks,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Top',$missing->missing->top,['readonly'=>'readonly']) }}
                </div>
                <div class="col-xs-4">
                    {{ Form::bsText(null,'Bottom',$missing->missing->bottom,['readonly'=>'readonly']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="form-group">
                        <label for="">Last Known Location</label>
                        <input hidden type="text" name="" id="lat" value="{{$missing->missing->lat}}">
                        <input hidden type="text" name="" id="lng" value="{{$missing->missing->lng}}">
                        <div id="mapMissingDetail" style="height:50vh;">  </div>
                    </div>
                </div>
                <div class="col-xs-4">
                        <div class="form-group">
                            <label for="">Description</label>
                            <textarea readonly name="description" class="form-control" style="resize:none; height:15vh;">{{$missing->missing->description}}</textarea>
                        </div>
                        <a href="{{url('/missings/print_details/'.$missing->missing->id)}}" class="btn btn-default">Download PDF</a>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection