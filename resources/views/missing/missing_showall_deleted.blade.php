@extends('app.template')
@section('content')
<div class="col-xs-12">

    @if(Session::has('msg'))
        <div class="alert alert-info">
            <a class="close" data-dismiss="alert">×</a>
            <strong>Heads Up!</strong> {{Session::get('msg')}}
        </div>
    @endif

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Show all deleted Missing Persons in this station.</h3>
        </div>

        <div class="box-body">
            <table id="Table" class="table table-hover table-responsive">
                <thead>
                <tr>
                    <th>Image</th>
                    <th>Missing Person Name</th>
                    <th>Pettitioner's Name</th>
                    <th>Date Created</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($missings AS $missing)
                    <tr>
                        <td ><center><img  id="missingPerson_image" src="/uploads/missings/{{$missing->missing->avatar}}" width="60" height="60" alt="Image of the missing person"></center></td>
                        <td>{{$missing->missing->first_name}}&nbsp{{$missing->missing->middle_name}}&nbsp{{$missing->missing->last_name}}</td>
                        <td>{{$missing->petitioner->first_name}}&nbsp{{$missing->petitioner->middle_name}}&nbsp{{$missing->petitioner->last_name}}</td>
                        <td>{{$missing->missing->created_at}}</td>
                        <td>{{($missing->flag)? "": "Deleted"}}</td>
                        <td>
                            <form action="{{url('missings/restoreMissing/'.$missing->missing_id)}}" method="post">
                                {{csrf_field()}}
                                <button type="submit" onclick="activation(event)" class="btn btn-danger btn-block btn-md">Restore Missing Person</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection




