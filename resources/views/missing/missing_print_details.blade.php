<!doctype html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
@php
    $birthdate = $missing->missing->birthdate;
    $date = new DateTime($birthdate);
    $now = new DateTime();
    $age = $date->diff($now)->format("%y");
@endphp
<div class="container">
    <center><h1 style="font-family: 'Raleway', sans-serif; font-size: 72px;"><strong>MISSING PERSON</strong></h1></center>
    <br>
    <br>
    <center><img src="{{asset('uploads/missings/'.$missing->missing->avatar)}}" alt=""></center>
    <br>
    <hr>
    <br>
    <div style="font-family: 'Raleway', sans-serif; font-size: 24px;">
        <strong>Name: </strong> {{$missing->missing->first_name.' '.$missing->missing->last_name}}<br>
        <strong>Missing since: </strong>{{$missing->missing->date_last_seen}}<br>
        <strong>Please Contact: </strong>{{$missing->petitioner->contact_no}}<br>
        <strong>Age: </strong>{{$age}} years old <br>
        <strong>Top: </strong>{{$missing->missing->top}}<br>
        <strong>Bottom: </strong>{{$missing->missing->top}}<br>
        <strong>Medical History: </strong>{{$missing->missing->medical_history}}<br>


    </div>
</div>
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- googlejs -->
<script src="{{asset('js/googlemap.js')}}"></script>
<!-- Google Maps Api -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxSQi5f9191VWRXUXAfbQ1erQ74fqf9b4&libraries=places&callback=initAutocomplete"
        async defer></script>
</body>

</html>