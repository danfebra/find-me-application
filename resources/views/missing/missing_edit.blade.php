@extends('app.template')
@section('content')
  @if(Session::has('msg'))
    <div class="alert alert-info">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Heads Up!</strong> {{Session::get('msg')}}
    </div>
  @endif


<div class="col-xs-12">
  <div class="box">
    <div class="box-header">
      <h3 class="box-title">Edit Missing Person Record</h3>
    </div>

    <div class="box-body">
      <!-- Form for updating only the image of the missing person without touching the other information of the missing person -->
      <form class=""  enctype="multipart/form-data"  action="{{url('missings/updateAvatar/'.$missing->id)}}" method="post">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              {{Form::bsFile('avatar','Update Avatar',null)}}
              <button type="submit" name="button" class="btn btn-success btn-sm pull-right">Update Image</button>
            </div>
          </div>
        </div>
        <hr>
      </form>

      <br>

      <center><img src="{{asset('uploads/missings/'.$missing->avatar)}}" width="300" height="300" alt="" style="border-radius: 50%;"></center>

      <br>

      <hr>
      <form action="{{url('/missings/updateMissing/'.$missing->id)}}" method="post">
        {{csrf_field()}}

        <div class="row">
          <div class="col-xs-4">
            {{ Form::bsText('first_name','First Name',$missing->first_name,['placeholder'=>'First Name','id'=>'first_name']) }}
          </div>
          <div class="col-xs-4">
            {{ Form::bsText('middle_name','Middle Name',$missing->middle_name,['placeholder'=>'Middle Name','id'=>'middle_name']) }}
          </div>
          <div class="col-xs-4">
            {{ Form::bsText('last_name','Last Name',$missing->last_name,['placeholder'=>'Last Name','id'=>'last_name']) }}
          </div>
        </div>

        <div class="row">
            <div class="col-xs-3">
              {{ Form::bsDate('birthdate','Date of Birth',$missing->birthdate) }}
            </div>
            <div class="col-xs-3">
              {{ Form::bsSelect('gender','Gender',['Male'=>'Male','Female'=>'Female'],$missing->gender) }}
            </div>
            <div class="col-xs-3">
              {{ Form::bsDate('date_last_seen','Date Last Seen',$missing->date_last_seen) }}
            </div>
            <div class="col-xs-3">
              {{ Form::bsText('nationality','Nationality',$missing->nationality,['placeholder'=>'Nationality']) }}
            </div>
        </div>

        <div class="row">
          <div class="col-xs-4">
              {{ Form::bsSelect('skin_type','Skin Type',['Fair'=>'Fair','Dark'=>'Dark','Olive'=>'Olive'],$missing->skin_type) }}
          </div>
          <div class="col-xs-2">
              {{ Form::bsText('height','Heigth',$missing->height,['placeholder'=>'Height']) }}
          </div>
          <div class="col-xs-2">
              {{ Form::bsText('weight','Weight',$missing->weight,['placeholder'=>'Weight']) }}
          </div>
          <div class="col-xs-4">
              {{ Form::bsText('medical_history','Medical History',$missing->medical_history,['placeholder'=>'medical_history']) }}
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <div class="form-group {{$errors->has('body_marks')? "has-error": "" }}">
              <label for="">Body Marks</label>
              <select id="BodyMarks" name="body_marks" class="form-control">
                @if($missing->body_marks == "None")
                <option value="None" id="none">None</option>
                <option value="Tattoo" id="tattoo">Tattoo</option>
                @else
                <option value="Tattoo" id="tattoo">Tattoo</option>
                <option value="None" id="none">None</option>
                @endif
              </select>
            </div>
          </div>
          <div class="col-xs-4">
            {{ Form::bsText('top','Top',$missing->top,['placeholder'=>'E.g. Red Shirt']) }}
          </div>
          <div class="col-xs-4">
            {{ Form::bsText('bottom','Bottom',$missing->bottom,['placeholder'=>'E.g. Black cargo shorts']) }}
          </div>
        </div>
        <div class="row">
          <div class="col-xs-4">
            <div class="form-group">
              <label for="">Description</label>
              <input type="text" hidden value="{{$missing->description}}" id="first_description">
              <textarea name="description" id="Description" class="form-control" style="resize:none; height:39vh;">{{$missing->description}}</textarea>
            </div>
          </div>

          <div class="col-xs-8">
            <input type="text" hidden name="lat" id="lat" value="{{$missing->lat}}">
            <input type="text" hidden name="lng" id="lng" value="{{$missing->lng}}">
            <div class="form-group">
              <label for="">Last known location</label>
                <input type="text" class="form-control" placeholder="Search..." id="searchbox" value="">
              <div id="mapEditMissing" style="height:35vh;">

              </div>
            </div>
          </div>
        </div>
        <button type="submit" name="button" class="btn btn-success pull-right">Update</button>
      </form>
      <form action="{{url('/missings/foundORmissing/'.$missing->id)}}" method="POST">
        {{csrf_field()}}
        <button type="submit" class="btn btn-danger">Found</button>
      </form>
    </div>
  </div>
</div>

@endsection
