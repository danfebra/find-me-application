<!DOCTYPE html>
<html>
<head>

  <audio controls autoplay hidden>
  <source src="{{asset('assets/hayaans.mp3')}}" type="audio/mp3">
  </audio>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Find Me | Login</title>

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('assets/bower_components/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/dist/css/AdminLTE.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('assets/plugins/iCheck/square/blue.css')}}">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <link rel="icon" type="image/x-icon" href="{{ asset('assets/icon/icon.png') }}">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="container-fluid">
      <div class="showImage">
      <img id="qrCodeImg" hidden style="position:fixed; z-index: 1; margin-top: 219px; margin-left:130vh; " src="{{asset('assets/qrcode.png')}}" alt="">
      </div>
</div>
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Find</b><img src="{{asset('assets/icon/icon.png')}}" width="50" height="50" alt="">ME</a>
    <p style="font-size:15px;">A web and mobile base communication system for missing persons in selected Barangays in Cebu City</p>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>
  
    <form action="{{url('/doLogin')}}" method="post">
      {{csrf_field()}}
      <div class="form-group has-feedback {{$errors->has('username')? 'has-error':''}}">
        <input type="text" class="form-control" name="username" placeholder="Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
        @if($errors->has('username'))
        <p>*{{$errors->first('username')}}</p>
        @endif
      </div>
      <div class="form-group has-feedback {{$errors->has('username')? 'has-error':''}}">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if($errors->has('password'))
        <p>*{{$errors->first('password')}}</p>
        @endif
      </div>
      <div class="row">

        <a href="javascript:showQrCode();" style="font-size: 8px;" class="pull-right" >Scan to download the app.</a>
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </div>
    </form>
  </div>

  <!-- /.login-box-body -->
</div>
<script src="{{asset('assets/bower_components/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('assets/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>

<script>

    function showQrCode() {
        $('.showImage').each(function() {
            if ($("#qrCodeImg").attr("hidden")) {
                $("#qrCodeImg").attr("hidden",false);
            }
            else {
                $("#qrCodeImg").attr("hidden",true);
            }
        });
    }


</script>
</body>
</html>
