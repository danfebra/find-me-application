@extends('app.template')
@section('content')
  @if(Session::has('msg'))
    <div class="alert alert-info">
      <a class="close" data-dismiss="alert">×</a>
      <strong>Heads Up!</strong> {{Session::get('msg')}}
    </div>
  @endif
<div class="box">
  <div class="box-header">
    <h3 class="box-title">All Reported Missing Persons</h3>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <div class="col-xs-12">
      <table id="Table" class="table table-bordered table-striped table-hover table-responsive">
        <thead>
        <tr>
          <th>Missing Person's Photo</th>
          <th class="col-xs-2">Missing Person's name</th>
          <th>Petitioner's Name</th>
          <th>Reported in Station</th>
          <th>Recorded by</th>
          <th>Status</th>
          <th>Action</th>

        </tr>
        </thead>
        <tbody>

        @foreach($missings AS $missing)
          <tr style="color: #161616">
            <td><center><img src="/uploads/missings/{{$missing->missing->avatar}}" width="100" height="100"></center></td>
            <td>{{$missing->missing->first_name}} {{$missing->missing->middle_name}} {{$missing->missing->last_name}}</td>
            <td>{{$missing->petitioner->first_name}} {{$missing->petitioner->middle_name}} {{$missing->petitioner->last_name}}</td>
            <td>{{$missing->station->name}}</td>
            <td>{{$missing->officer->rank}} {{$missing->officer->first_name}} {{$missing->officer->middle_name}} {{$missing->officer->last_name}} of {{$missing->station->name}}</td>
            <td>{{ ($missing->status)? "Missing":"Found" }}</td>

            <td>
              <center>

                <a  href="{{url('/missings/missingPersonDetail/'.$missing->missing_id)}}" class="btn btn-sm btn-block btn-success" name="button">Details</a>



                <a style=" display: {{($missing->station_id == Auth::guard('officer')->user()->station_id) &&  $missing->status? "" :"none" }}"  href="{{url('/missings/editMissing/'.$missing->missing_id)}}" class="btn btn-sm btn-block btn-primary" name="button">Edit</a>



                <form action="{{url('/missings/deleteMissing/'.$missing->missing_id)}}" method="POST" style="margin-top: 5px; display: {{($missing->station_id == Auth::guard('officer')->user()->station_id) &&  $missing->status? "" :"none"}} " href="{{url('/missings/deleteMissing/'.$missing->missing->id)}}">
                  {{csrf_field()}}
                  <input type="text" name="flag" hidden value="0">
                  <button type="submit" class="btn btn-sm btn-block btn-danger" onclick="activation(event)">Delete</button>
                </form>



              </center>
            </td>

          </tr>
        @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <!-- /.box-body -->
</div>


@endsection
